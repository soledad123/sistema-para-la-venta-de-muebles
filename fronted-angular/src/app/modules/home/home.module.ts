import { NgModule } from '@angular/core';
import { PrincipalComponent } from './principal/principal.component';
import { SharedModule } from '@shared/shared.module';
import { HomeRoutingModule } from './principal/home-routing.module';



@NgModule({
  declarations: [
    PrincipalComponent
  ],
  imports: [
    SharedModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
