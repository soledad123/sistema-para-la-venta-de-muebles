import { NgModule } from '@angular/core';
import { ListPublicationsComponent } from './list-publications/list-publications.component';
import { CreatePublicationsComponent } from './create-publications/create-publications.component';
import { SharedModule } from '@shared/shared.module';
import { PublicationsRoutingModule } from './publications-routing.module';



@NgModule({
  declarations: [
    ListPublicationsComponent,
    CreatePublicationsComponent
  ],
  imports: [
    SharedModule,
    PublicationsRoutingModule
  ]
})
export class PublicationsModule { }
