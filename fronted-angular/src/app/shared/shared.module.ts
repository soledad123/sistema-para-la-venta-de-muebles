import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as fromComponents from './components'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http'



@NgModule({
  imports: [
    FormsModule,// para los formularios
    ReactiveFormsModule,//variables reativas 
    HttpClientModule,//para comunicardos con el restapi -> GO -> backend
   CommonModule
  ],
  declarations:[
    ...fromComponents.components
  ],
  exports:[
    FormsModule,// para los formularios
    ReactiveFormsModule,//variables reativas 
    HttpClientModule,//para comunicardos con el restapi -> GO -> backend
    CommonModule,
    ...fromComponents.components
  ]

})

export class SharedModule { }
