import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as AOS from 'aos'

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class SkeletonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    AOS.init()
  
  }

}
